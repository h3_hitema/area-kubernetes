c# Area Kubernetes


## API Reference

#### Get all area

```http
  GET /api/areas
```

#### Post area

```http
  POST /api/areas/create
```

```json
{
  "name" : "Laboratoire",
  "price" : 150000
}
```

#### Patch area

```http
  PATCH /api/areas/update/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- |:----------------------------------|
| `id`      | `string` | **Required**. Id of area to patch |

```json
{
  "name" : "Entrepôt",
  "price" : 180000
}
```


## 🛠 Skills
NodeJs, Javascript, Mongo DB, and Docker


## Authors

- [@Marc Baribaud](https://gitlab.com/devFendrix)
- [@Edouard Quilliou](https://gitlab.com/EdouardQ)

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`MONGODB_USER`
`MONGODB_PASSWORD`
`MONGODB_PORT`
`MONGODB_DOCKER_PORT`
