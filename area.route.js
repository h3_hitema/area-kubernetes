const AreaController = require("./area.controller")
const { Router } = require("express");

const router = Router()

router.get("/areas", AreaController.areas)
router.post("/areas/create", AreaController.create)
router.patch("/areas/update/:id", AreaController.update)

module.exports = router